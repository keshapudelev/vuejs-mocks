# vuejs-mocks

# Приложение

Это дефолтное приложение, созданное через Vue CLI.

К нему добавлена ещё одна вкладка с приложением Todo-list, в которой демонстрируется работа с мок-сервером.

![screenshot](public/img/todos.png)

Текущее состояние мок-режима отображается в скобках в названии третьей вкладки.

Как запустить приложение в мок-режиме описано дальше.

![screenshot](public/img/mocks.png)



Записи todo-листа хранятся в базе.

Запросы к api можно отслеживать в dev-tools.

![screenshot](public/img/dev-tools.png)



# Установка зависимостей
```
npm i
```
***node v12.8.1** 


### Запуск приложения локально (без моков)

Проект по умолчанию запускается на `localhost:8080`.

```
npm run serve
```

---

# MOCKS

### Запуск приложения с api на мок-сервере

В качестве сервера api используется [json-server](https://github.com/typicode/json-server).

По умолчанию мок-сервер запускается на `localhost:4242`.

По умолчанию приложение запускается на `localhost:2020`.

```
npm run serve-mocks
```
Команда запускает мок-сервер и само приложение в режиме `--mode mocks`, приложение будет обращаться за api к мок-серверу.

### Мок-сервер

Основная база находится в `mocks/db.json`.

Настройки сервера в `mocks/server.js`.

Конфиг json-server в `mocks/json-server.json`.

Запуск происходит через [forever](https://github.com/foreversd/forever).

При изменении данных в базе `db.json` перезапуск мок-сервера не требуется.

При изменении настроек сервера - его следует перезапустить.

##### Перезапуск мок-сервера (отдельно от приложения)
```
npm run mock-server:restart
```

##### Остановка мок-сервера (отдельно от приложения)
```
npm run mock-server:stop
```

##### Запуск мок-сервера (перед применением - необходимо остановить запущенный сервер)
```
npm run mock-server:start
```

---

## BUILD and LINT

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
