# json-server

Для моков используется nodejs-сервер [json-server](https://github.com/typicode/json-server).

Основная база находится в `./db.json`.

Настройки сервера в `./server.js`.

Конфиг json-server в `./json-server.json`.
