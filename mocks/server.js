const jsonServer = require('json-server')
const path = require('path')
const server = jsonServer.create()

// get default routes with db.json
const router = jsonServer.router(path.join(__dirname, 'db.json'))
const middlewares = jsonServer.defaults()

const {
  port
} = require('./json-server.json');

server.use(middlewares)
server.use(jsonServer.bodyParser)

server.use(router)

// server.use((req, res, next) => {
//   return next();
// })

server.listen(port, () => {
  console.log('JSON Server is running')
})
